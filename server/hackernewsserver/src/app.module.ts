import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { HnModule } from './hn/hn.module';
import { MongooseModule } from '@nestjs/mongoose';


@Module({
  imports: [HnModule, MongooseModule.forRoot('mongodb://172.155.134.4:27017/xfrancisc0_hn')],
  controllers: [AppController],
  providers: [AppService]
})
export class AppModule {}
