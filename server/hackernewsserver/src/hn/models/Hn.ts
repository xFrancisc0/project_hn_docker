import { Schema } from 'mongoose';

export const HnSchema = new Schema({
    objectID: {
        type: Number,
        unique: true,
        required: false
    },
    name: {
        type: String,
        required: false
    },
    author: {
        type: String,
        required: false
    },
    date: {
        type: Date,
        required: false
    },
    story_url: {
        type: String,
        required: false
    },
    renabled: {
        type: Boolean,
        default: true
    }
});
