import { Document } from 'mongoose';

export interface Hn extends Document{
    objectID?: string;
    name: string;
    author: string;
    date: Date;
    story_url: string;
    renabled: boolean;
}