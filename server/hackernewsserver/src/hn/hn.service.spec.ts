import { Test, TestingModule } from '@nestjs/testing';
import { HnService } from './hn.service';

describe('HnService', () => {
  let service: HnService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [HnService],
    }).compile();

    service = module.get<HnService>(HnService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
