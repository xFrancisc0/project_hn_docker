import { Controller, Get, Delete, Param, Req, Res } from '@nestjs/common';
//import { CreateHNDto } from './dto/create-hn.dto'; //This would only be used for Create and Update operations.

import { HnService } from './hn.service'; //This is used for Database operations.

@Controller('hn')
export class HnController {

    constructor(private _hnservice: HnService){
        //Inmediatly after starting, remove everything from collection hns
        this.clean_collection();

        //VALUE
        //Then inmediatly refresh from database
        console.log(`Beggining synchronization.`);
        console.log(`--------------------------`);
        this.sync().then().catch(console.log);



        //Synchronize mongodb database with API each 1 hour. This is an infinite loop.
        console.log(`Beggining synchronization.`);
        console.log(`--------------------------`);
        this.constant_sync(1000 * 60 * 60)  // 1s * 60 (1m) * 60 (1h)
    }

    
    async constant_sync(x : number) {
        let intervalid;
        intervalid = setInterval(() => {
            this._hnservice.sync_func().then(res => {
            })  
        }, x)  
    }

    async clean_collection(){
        await this._hnservice.clean_collection();
    }

    async sync(){
        await this._hnservice.sync_func();
        
    }


    @Get('/hackernews/gethits')
    async getHNs() {
        let _hns = await this._hnservice.getHNs()
        return {"ok": true, "hns": _hns}

    }

    @Delete('/hackernews/deletehit/:id')
    updateHN(@Param() param){
     return this._hnservice.deleteHN(param.id);
    }

}
