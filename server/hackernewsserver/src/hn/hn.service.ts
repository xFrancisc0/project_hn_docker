import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Hn } from './interfaces/Hn';
import { Model } from 'mongoose';
import { HttpService } from '@nestjs/common';

@Injectable()
export class HnService {

    constructor(@InjectModel('Hn') private hnModel: Model<Hn>, private httpService: HttpService){

        
    }

    async getHNs(){
        return await this.hnModel.find({renabled : true});
    }

    async clean_collection(){
        await this.hnModel.find().remove();  //Remove all data from hns collection
        return 1;
    }
    async sync_func(): Promise<any> {


        

        let resp = await this.httpService.get('https://hn.algolia.com/api/v1/search_by_date?query=nodejs').toPromise();

        let _renabled = true;
        let _name = null;
        let _url = null;
        let nhits = resp.data.hitsPerPage;

        for (let i = 0; i < nhits; i++) {

            if (resp.data.hits[i].story_title == null && resp.data.hits[i].title == null) {
                _renabled = false;
                _name = null;
            } else if (resp.data.hits[i].story_title == null) {
                _name = resp.data.hits[i].title;
            } else {
                _name = resp.data.hits[i].story_title;
            }

            if (resp.data.hits[i].story_url == null && resp.data.hits[i].url == null) {
                _url = null
            } else if (resp.data.hits[i].story_url == null) {
                _url = resp.data.hits[i].url;
            } else {
                _url = resp.data.hits[i].story_url;
            }

            
            const hn = new this.hnModel({
                objectID: resp.data.hits[i].objectID,
                name: _name,
                author: resp.data.hits[i].author,
                date: resp.data.hits[i].created_at,
                story_url: _url,
                renabled: _renabled
            })


            await hn.save((error, registryDB) => {
                if (error) {
                    console.log(`While synchronicing, it was found that the ObjectID of an object is already in the database, no need to insert it again.`);
                } else {
                    console.log(`Successfully added:`);
                    console.log(registryDB);
                }
            });
        }

        return 1;
    }



    async deleteHN(id){
        let flag = 0;
        let _error = "";
        await this.hnModel.findByIdAndUpdate(id, {"renabled":false}, { new: true, runValidators: true }, (error, user) => {
            if (error) {
                _error = error;
            }else{
                flag = 1;
            }
        });

        if(flag == 0 ){
            return {
                "ok": false,
                "error": "There was an error"
            };
        }else{
            return{
                ok: true,
                mensaje: "Row deleted"
            };
        }

    }
}
