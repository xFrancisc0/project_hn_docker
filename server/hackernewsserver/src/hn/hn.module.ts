import { HttpModule, Module } from '@nestjs/common';
import { HnService} from './hn.service';
import { HnController} from './hn.controller'
import { MongooseModule } from '@nestjs/mongoose';
import { HnSchema } from './models/Hn';

@Module({
    imports: [HttpModule, 
             MongooseModule.forFeature([
                 {name: 'Hn', schema: HnSchema}
             ])
            ],
    controllers:[HnController],
    providers:[HnService]

})
export class HnModule {}