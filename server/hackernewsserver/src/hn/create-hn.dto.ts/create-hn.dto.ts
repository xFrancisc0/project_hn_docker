export class CreateHNDto{
    objectID?: string;
    name: string;
    author: string;
    date: Date;
    story_url: string;
    renabled: boolean;
}