import { Component, OnInit } from '@angular/core';
import { HnService } from '../../services/hn.service';
import { HnItem } from '../../models/HN';

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css']
})
export class IndexComponent implements OnInit {

  iconoEliminar: boolean;
  hns:any[] = [];
  constructor(private _hnService:HnService) { this.iconoEliminar = false; }

  ngOnInit(): void {
    this._hnService.loadData().subscribe( resp => {
      let result= JSON.parse(JSON.stringify(resp).replace(/_id/g,"id"));

      Object.keys(result.hns).forEach(key=>{
        const registro: HnItem = (<any>result.hns)[key];
        this.hns.push(registro);
      });
      return 0;
    });
  }

  DeleteHN(hn:HnItem){
    this._hnService.disableHN(hn);
    //Despues de eliminar la imagen se elimina tambien de la lista de imagenes

    let index = this.hns.findIndex(registro => {
      return registro.id === hn.id;
    });

  this.hns.splice(index, 1);//remove element from array

  }

}
