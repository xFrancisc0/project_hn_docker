export class HnItem{
    public id: string = "";
    public objectID: number;
    public name: string; 
    public author: string; 
    public date: Date; 
    public story_url: string; 
    public renabled: boolean;

    constructor(hn: HnItem){
        this.objectID = hn.objectID;
        this.name = hn.name;
        this.author = hn.author;
        this.date = hn.date;
        this.story_url= hn.story_url;
        this.renabled = hn.renabled;
    }
}