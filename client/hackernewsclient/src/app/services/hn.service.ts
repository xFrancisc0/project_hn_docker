import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { HnItem } from '../models/HN';

@Injectable({
  providedIn: 'root'
})
export class HnService {

  private url:string = "http://localhost:3000/hn";

  constructor(private http : HttpClient) { }

  public loadData(){
    return this.http.get(`${this.url}/hackernews/gethits`);
  }

  public disableHN(hn: HnItem){
    return this.http.delete(`${this.url}/hackernews/deletehit/${hn.id}` ).subscribe(() => console.log("Row deleted"));
  }

}
