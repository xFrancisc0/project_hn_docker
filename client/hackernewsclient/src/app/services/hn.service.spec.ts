import { TestBed } from '@angular/core/testing';

import { HnService } from './hn.service';

describe('HnService', () => {
  let service: HnService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(HnService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
