**Project HN**__

It is needed Docker and Docker-compose in order to make this project work.

First you need to copy all these files into a folder.

Then, using terminal write: docker-compose up -d

After some minutes the images will be generated and the containers will be greated.

This project doesn't use volumes because it is a small project. However, it does use a VPC because all the components need to be connected.




**If you need to change the directions of the VPC, modify:**

```
1.- docker-compose.yml: Replace subnet with something else (example: 150.130.120.0/24) and ipv4 addresses (example: 150.130.120.2 Database, 150.130.120.3 server, 150.130.120.4 client). Be sure to not to choose x.x.x.1 nor x.x.x.255 because those addresses are reserved..

2.- Due the size of this proyect, environment values were not used. That means if you did replace all the ipv4 addresses and subnet, be sure to make the changes in the affected files. 
The NestJS server is connected to the MongoDB's Guest Address. The client is pointing on the server's host IP address.
```


If for example:

```
MongoDB address: 150.130.120.2 VPC IP
NestJS address: 150.130.120.3 VPC IP <-------> Localhost
Client address: 150.130.120.4 VPC IP
```



Modify:

```
docker_project_hn\server\hackernewsserver\src\app.module.ts, line:  MongooseModule.forRoot('mongodb://150.130.120.2:27017/xfrancisc0_hn')]
#Server Access to VPC address to database

docker_project_hn\client\hackernewsclient\src\app\services\hn.service.ts, line: private url:string = "http://localhost:3000/hn";
#Client Access to server through localhost IP adress. If the client is in another net, you can access to the server through it's private or public IP address instead of localhost.
```


